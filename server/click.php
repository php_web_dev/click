<?php
/**
 *
 * 海报页面展示量和点击量计数器
 * User: fish
 * Date: 2016-12-01 00:17
 * click.php
 *
 * http://192.168.71.21:8005/click.php?a=sub_pv&promopage_plan_id=10&gateway_id=2&game_server_id=237&game_id=185&promopage_id=6&promopage_sub_id=81
 * http://192.168.1.30:8001/spread/Cron/click/click.php?a=pv&promopage_plan_id=10&gateway_id=2&game_server_id=237&game_id=185&promopage_id=6&promopage_sub_id=81
 *
 *
 */
 
$action = isset($_GET['a'])?$_GET['a']:'';
if (empty($action)) {
    exit('no action');
}

date_default_timezone_set('PRC');
require 'CacheRedis.class.php';
require 'RedisStatService.class.php';
define('LOG_PATH','/data/log/click/');
//define('LOG_PATH','./log/');
if(!is_dir(LOG_PATH)){
    mkdir(LOG_PATH,0755,true);
}
if(!is_dir(LOG_PATH.'pv')){
    mkdir(LOG_PATH.'pv',0755,true);
}
if(!is_dir(LOG_PATH.'sub_pv')){
    mkdir(LOG_PATH.'sub_pv',0755,true);
}
$GLOBALS['RedisStat'] = new RedisStatService();
$param  = array_merge($_GET,$_POST);
validate($param);
call_user_func($action, $param);

function validate($param){
    if($param['sign'] !=md5('xxxxxx@#$s'.$param['promopage_plan_id'])){
        exit('sign error');
    }
    //md5('menle@#$'.$promopage_plan['id']);
}

function pv($param){
    error_log(date('Y-m-d H:i:s').','.get_client_ip().','.implode(',',$param).PHP_EOL,3,LOG_PATH.'pv/'.date('Ymd').'.pv.log');

    $ip_key = 'ip'.ip2long(get_client_ip()).md5(http_build_query($param));
    $ip_has_statics = $GLOBALS['RedisStat']->redis->get($ip_key);
    if($ip_has_statics){
        return false;
    }


    $promopage_plan_id = (int)$param['promopage_plan_id'];
    $gateway_id     =   (int)$param['gateway_id'];
    $game_server_id =   (int)$param['game_server_id'];
    $game_id        =   (int)$param['game_id'];
    $promopage_id  =  (int)$param['promopage_id'];
    $promopage_sub_id    =  (int)$param['promopage_sub_id'];
    if(!empty($promopage_plan_id)
       &&!empty($gateway_id)
       &&!empty($game_server_id)
       &&!empty($game_id)
       &&!empty($promopage_id)
       &&!empty($promopage_sub_id)
    ){
        $GLOBALS['RedisStat']->init_stat($promopage_plan_id,$gateway_id,$game_id,$game_server_id,$promopage_id,$promopage_sub_id)->pv();
        $GLOBALS['RedisStat']->redis->set($ip_key,1,86400);
    }
}
function sub_pv($param){
    error_log(date('Y-m-d H:i:s').','.get_client_ip().','.implode(',',$param).PHP_EOL,3,LOG_PATH.'sub_pv/'.date('Ymd').'.sub_pv.log');
    $ip_key = 'ip_sub_pv'.ip2long(get_client_ip()).md5(http_build_query($param));
    $ip_has_statics = $GLOBALS['RedisStat']->redis->get($ip_key);
    if($ip_has_statics){
        return false;
    }

    $promopage_plan_id = (int)$param['promopage_plan_id'];
    $gateway_id     =   (int)$param['gateway_id'];
    $game_server_id =   (int)$param['game_server_id'];
    $game_id        =   (int)$param['game_id'];
    $promopage_id  =  (int)$param['promopage_id'];
    $promopage_sub_id    =  (int)$param['promopage_sub_id'];
    if(!empty($promopage_plan_id)
        &&!empty($gateway_id)
        &&!empty($game_server_id)
        &&!empty($game_id)
        &&!empty($promopage_id)
        &&!empty($promopage_sub_id)
    ){
        $GLOBALS['RedisStat']->init_stat($promopage_plan_id,$gateway_id,$game_id,$game_server_id,$promopage_id,$promopage_sub_id)->sub_pv();
        $GLOBALS['RedisStat']->redis->set($ip_key,1,86400);
    }
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @return mixed
 */
function get_client_ip($type = 0) {
	$type       =  $type ? 1 : 0;
    static $ip  =   NULL;
    if ($ip !== NULL) return $ip[$type];
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $pos    =   array_search('unknown',$arr);
        if(false !== $pos) unset($arr[$pos]);
        $ip     =   trim($arr[0]);
    }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip     =   $_SERVER['HTTP_CLIENT_IP'];
    }elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip     =   $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u",ip2long($ip));
    $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}

